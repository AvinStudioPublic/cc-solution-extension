'use strict';

module.exports = {
  load () {
    // When the package loaded
  },

  unload () {
    // When the package unloaded
  },

  messages: {
    'open-nodes' () {
	  Editor.Panel.open('avin-studio');
    },
  },
};