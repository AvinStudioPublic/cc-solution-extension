// panel/index.js
'use strict';
//import _ from "../node_modules/underscore/underscore-min"
const Fs = require('fs');
const Path = require('path');
let utils = require(Editor.url("packages://avin-studio/panel/utils"));

var _require = function(e) {return Editor.require(e)};
var readAsset=(fs,p)=>{return fs.readFileSync(Editor.url(p),"utf8");};
var log=(a)=>{Editor.Scene.callSceneScript('avin-studio', 'console-log',a);};

Editor.Panel.extend({
  style: readAsset(Fs,"packages://avin-studio/panel/style.css"),
  template: readAsset(Fs,"packages://avin-studio/panel/panel.html"),
  code:readAsset(Fs,"packages://package-asset/panel/import/panel.html"),
  vm:{},
  messages:{
    "builder:state-changed":function(event, ...args){
      if(args[0]=="finish"){
        if(!this.vm.selectedProject.customBuild)return;
        this.vm.onBuildClick();
      }
    }
  },

  ready () {
    var self = this;
    utils.init(this.profiles.project);
    this.project = this.profiles.project.data;
    //ViewModel
    self.vm =  new window.Vue({
      el: this.shadowRoot,
      data: {
        tabs:[{title:"Settings",active:false},{title:"Options",active:false},],
        tabIdx: 0,
        showLoading:true,
        message:this.profiles.project.data.name,
        project:this.profiles.project,
        projects:this.profiles.project.data.projects,
        showDeleteDialog:false,
        selectedProject:{id:"",name:"",buildPath:"",settingUuid:"",sceneUuid:"",projectPath:"",customBuild:false}
      },
      methods: {
        _onTabClick($index){
          this.tabIdx = $index;
        },
        _newProject(){this.selectedProject = {id:"",name:"",buildPath:"",settingUuid:"",projectPath:"",sceneUuid:"",customBuild:false}},
        onChooseBuildPath(){
          utils.showBuildOutPathDialog((e,t)=>{
                if(e){Editor.error(e);
                  return;
                }
                this.selectedProject.buildPath = t;
              }
          )

        },
        onAddToProjectsClick(){
          if(this.selectedProject.id=="") {
            this.selectedProject.id = utils.uuid();
            this.project.data.projects.push(this.selectedProject);
          }
          log(this.selectedProject)
          log(this.project)
          this.project.save();

          //this._newProject();
        },
        onOpenPathClick(){
          utils.openPath(this.selectedProject.buildPath);
        },
        onNewClick(){
          this._newProject();
        },
        onDeleteItemClick(){
          if(this.selectedProject.id=="") return;
          this.showDeleteDialog = true;
        },
        onDeleteClick(v){
          this.showDeleteDialog = false;
          if(!v)return;
          var ind =0;
          for(let item of this.projects){
            if(item.id==this.selectedProject.id)break;
            ind++
          }
          this.projects.splice(ind,1);
          this.project.save();
        },
        onApplyClick(v){
          this.selectedProject.isSelected=false;
          this.selectedProject = v;
          this.selectedProject.isSelected=true;
          if(v.sceneUuid!=""){
            Editor.Scene.callSceneScript ('avin-studio', 'confirm-close-scene');
            Editor.Scene.callSceneScript ('avin-studio', 'open-scene',v.sceneUuid,function(err,l) {
              Editor.log (`Scene Loaded : ${v.name}`);
            });
          }
          Editor.assetdb.queryInfoByUuid( v.settingUuid, ( err, info )=>{
            if(!info || !info.url)return
            var a = readAsset(Fs,info.url);
            this.projectSetting._data = JSON.parse(a);
            this.projectSetting.save();
            Editor.success("Project settings updated .Reopen the project to affect changes.")
            Editor.Ipc.sendToWins("editor:project-profile-updated", this.projectSetting.data);
          });
          var builder = Editor.remote.Profile.load("profile://local/builder.json");
          builder._data.buildPath = v.buildPath;
          builder.save();

          //save builder
          var scenes = [];
          for(var item of this.project.data.projects)
            if(item.sceneUuid!=v.sceneUuid)
              scenes.push(item.sceneUuid);
          this.builder._data.excludeScenes = scenes;
          this.builder._data.startScene = v.sceneUuid;
          this.builder.save();

          Editor.Ipc.sendToMain("project-settings:save-simulator-config", utils.configPath, utils.simulator_config);
          //utils.save("selectedProject",v.id);
          // this.project.data.selectedProject = "selectedProject"
          //this.project.save();

        },
        onBuildClick(){
          if(!this.selectedProject){
            Editor.warn("For build avin studio games you must select a project.")
            return;
          }
          this._readScriptFiles(this.selectedProject.name);
        },
        _readScriptFiles(filter){
          this.names = "";
          this.codes = "";
          Editor.assetdb.queryAssets( `db://assets/**\/${filter}*`, ['coffeescript','javascript'], ( err, results )=>{
            results.forEach((r)=> {
              var n= `${Path.basename(Path.basename(r.url,'.js'),'.coffee')}`;
              this.codes += `"${n}": [ function(require, module, exports) {${Fs.readFileSync(r.destPath,"utf8")}}, {} ],\r\n`;
              this.names += `"${n}",`
            });
            Editor.assetdb.queryAssets( `db://assets/**\/game*`, ['coffeescript','javascript'], ( err, results )=> {
              results.forEach((r)=> {
              var n = `${Path.basename(Path.basename(r.url, '.js'), '.coffee')}`;
              this.codes += `"${n}": [ function(require, module, exports) {${Fs.readFileSync(r.destPath, "utf8")}}, {} ],\r\n`;
              this.names += `"${n}",`
              });

              var code = `require = function e(t, n, r) {function s(o, u){if (!n[o]) {if (!t[o]) {var a = "function" == typeof require && require;if (!u && a) return a(o, !0);if (i) return i(o, !0);var f = new Error("Cannot find module '" + o + "'");throw f.code = "MODULE_NOT_FOUND", f;}var l = n[o] = {exports: {}};t[o][0].call(l.exports, function(e) {var n = t[o][1][e];return s(n || e);}, l, l.exports, e, t, n, r);}return n[o].exports;}var i = "function" == typeof require && require;for (var o = 0; o < r.length; o++) s(r[o]);return s;}
              ({${this.codes}},{},[${this.names}])`;
              var p = `${this.selectedProject.buildPath}web-mobile/src/${Editor.remote.Profile.load("profile://local/builder.json")._data.debug?"project.dev.js":"project.js"}`;
              Fs.writeFileSync(p,code,{encoding :"utf8"});
              Editor.success("Custom build success!");
            })

          });
        }
      },
      created:function(){
        Editor.Profile.load("profile://project/project.json", (e,t)=>{this.projectSetting = t;this.showLoading=false;})
        Editor.Profile.load("profile://project/builder.json", (e,t)=>{this.builder = t;
          //set solution to last project
          // if(self.project.selectedProject)
          // {
          //   for(var p of self.project.projects)
          //     if(p.id == self.project.selectedProject)
          //       this.onApplyClick(p)
          // }
        });
      },
    });
  },
});