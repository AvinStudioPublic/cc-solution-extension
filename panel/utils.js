/**
 * Created by S.Mahdi on 10/13/2017.
 */
'use strict';
const Electron = require("electron");
const dialog = Electron.remote.dialog;
const Path = require("fire-path")
const buildUtils = require(Editor.url("app://editor/share/build-utils"))
const Fs = require('fs');

exports.init = (t)=> {
    this.localProfiles = t
};

exports.save = (t, e)=> {
    this.localProfiles.data[t] = e;
    this.localProfiles.save()
};

exports.showImportJsonDialog = (t)=>{
    dialog.showOpenDialog({
            defaultPath: this.localProfiles.data["import-json-folder-path"] || Editor.projectInfo.path,
            properties: ["openFile"],
            filters: [{name: "JSON",extensions: ["json"]}]
        }, e=>{
            if (!t || !e) return;
            let o = e[0];
            t(null , o);
            this.save("import-json-folder-path", o);
        }
    )
};

exports.showBuildOutPathDialog = (t)=> {
    dialog.showOpenDialog({
            defaultPath: this.localProfiles.data["build-path"] || Editor.projectInfo.path,
            properties: ["openDirectory"]
        }, e=>{
            if (!t || !e)
                return;
            let o = Path.join(e[0], "/");
            t(null , o),
                this.save("build-path", o)
        }
    )
};

exports.openPath=(p)=>{
    var t = buildUtils.getAbsoluteBuildPath(p)
    if (!Fs.existsSync(t)){
        Editor.warn("%s not exists!", t);
        return;
    }
    Electron.shell.showItemInFolder(t);
    Electron.shell.beep();
};

exports.uuid = ()=> {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}
