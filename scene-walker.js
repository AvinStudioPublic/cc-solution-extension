/**
 * Created by S.Mahdi on 10/13/2017.
 */
module.exports = {
    "open-scene": function (event,uuid) {
        _Scene.loadSceneByUuid(uuid,(err)=>{Editor.log(err)});
        if(event.reply)
            event.reply("Scene Loaded");
    },
    "console-log":function(e,a){console.log(a)},
    "confirm-close-scene":function(event,a){
        _Scene.confirmClose()
    }

};